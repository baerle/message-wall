import React from "react";

export default class MessageList extends React.Component {
	render() {
		if (this.props.messages) {
			return (
				<ul id="message_list">
					{this.props.messages.map((message) => {
						return (
							<li className="message" key={message.id}>
								{message.content}
								<br />
								{message.date}
								<br />
								<button id="delete" onClick={() => this.props.handleDelete(message.id)}>Delete</button>
							</li>
						);
					})}
				</ul>
			);
		} else {
			return <ul id="message_list">no messages</ul>;
		}
	}
}

import React from "react";

export default class MessageForm extends React.Component {
	constructor() {
		super();
		this.state = {
			currentMessage: "",
		};
	}

	changeMessageValue(change) {
		this.setState({
			currentMessage: change,
		});
	}

	processSubmit(e) {
		e.preventDefault();
        this.props.submitMessage(this.state.currentMessage);
		this.changeMessageValue("");
	}

	render() {
		return (
			<form ref="formRef" onSubmit={(e) => this.processSubmit(e)}>
				<textarea
					id="message_box"
					onChange={(e) => this.changeMessageValue(e.target.value)}
					value={this.state.currentMessage}
				></textarea>
				<br />
				<button type="button" name="submit" id="submit">
					Submit
				</button>
			</form>
		);
	}

	/* componentDidUpdate() {
		console.log(this.state.currentMessage);
	} */
}

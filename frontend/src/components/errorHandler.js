import React from "react";

export default class ErrorHandler extends React.Component {
	render() {
		let result;
		if (this.props.error) {
			result = `Error: ${this.props.error.response.data}`;
		}
		return <div id="error">{result}</div>;
	}
}

import React from "react";
import MessageList from "./components/messageList";
import MessageForm from "./components/messageForm";
import ErrorHandler from "./components/errorHandler";
import axios from "axios";

import "./App.css";

const PORT = "http://localhost:3001";

export default class MessageApp extends React.Component {
	constructor() {
		super();
		this.state = {
			messages: [],
			error: "",
		};
	}

	componentDidMount() {
		this.getAllMessages();
	}

	getAllMessages() {
		axios
			.get(`${PORT}/`)
			.then((result) => this.setMessages(result.data))
			.catch((err) => {
				this.setError(err);
			});
	}

	submitMessage(data) {
		axios
			.post(`${PORT}/message`, { content: data })
			.then(() => {
				this.getAllMessages();
			})
			.catch((err) => {
				this.setError(err);
			});
	}

	deleteMessage(id) {
		axios
			.delete(`${PORT}/message/${id}`, { id: id })
			.then((result) => {
				this.getAllMessages();
			})
			.catch((err) => {
				this.setError(err);
			});
	}

	setError(error) {
		this.setState({ error: error });
	}

	setMessages(messages) {
		this.setState({ messages: messages });
	}

	render() {
		return (
			<div className="App">
				<ErrorHandler error={this.state.error} />
				<MessageForm ref="messageFormRef" submitMessage={this.submitMessage} />
				<MessageList messages={this.state.messages} handleDelete={this.deleteMessage} />
			</div>
		);
	}
}

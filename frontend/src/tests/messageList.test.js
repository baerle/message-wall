import React from "react";
import Enzyme from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import { mount, shallow } from "enzyme";
import MessageList from "../components/messageList";
import mockMessages from "../__mocks__/messages.json";

Enzyme.configure({ adapter: new Adapter() });

describe("List", () => {
	it("renders without crashing", () => {
		const component = mount(<MessageList />);
		expect(component).toMatchSnapshot();
	});

	it("takes messages as props and displays them", () => {
		const component = shallow(<MessageList messages={mockMessages} />);
		expect(component.find("ul#message_list").children().length).toBe(5);
	});

	it("each message in list has delete button", async () => {
		const component = await mount(<MessageList messages={mockMessages} />);
		expect(
			component.find("ul#message_list").childAt(0).exists("button#delete")
		).toBe(true);
	});
});

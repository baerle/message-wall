import React from "react";
import ReactDOM from "react-dom";
import axios from "axios";

import MessageApp from "../App";

import mockMessages from "../__mocks__/messages.json";
import errorMock from "../__mocks__/error.json";

import Enzyme from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import { mount } from "enzyme";

Enzyme.configure({ adapter: new Adapter() });

jest.mock("axios");

describe("MessageApp", () => {
	beforeEach(function () {
		axios.get.mockImplementation(() => Promise.resolve({ data: mockMessages }));
		axios.post.mockImplementation(() => Promise.resolve({ data: [] }));
		axios.put.mockImplementation(() => Promise.resolve({ data: [] }));
		axios.patch.mockImplementation(() => Promise.resolve({ data: [] }));
		axios.delete.mockImplementation(() => Promise.resolve({ data: [] }));
	});

	afterEach(() => {
		axios.post.mockClear();
	});

	it("renders without crashing", () => {
		const component = mount(<MessageApp />);
		expect(component).toMatchSnapshot();
	});

	it("has textbox", () => {
		const component = mount(<MessageApp />);
		expect(component.exists("textarea#message_box")).toBe(true);
	});

	it("has submit button", () => {
		const component = mount(<MessageApp />);
		expect(component.exists("button#submit")).toBe(true);
	});

	it("has message list", () => {
		const component = mount(<MessageApp />);
		expect(component.exists("ul#message_list")).toBe(true);
	});

	it("posts data and clears message box on submit success", () => {
		const component = mount(<MessageApp />);
		component.find("textarea#message_box").simulate("change", {
			target: { value: "Hello" },
		});
		component.find("form").simulate("submit");

		expect(axios.post).toHaveBeenCalledWith("http://localhost:3001/message", {
			content: "Hello",
		});

		expect(
			component.instance().refs.messageFormRef.state.currentMessage
		).toEqual("");
	});

	it("Loads data from api", () => {
		mount(<MessageApp />);
		expect(axios.get).toHaveBeenCalledTimes(1);
	});

	it("removes message on delete", async () => {
		const component = await mount(<MessageApp />);
		await component.update();
		console.log(component.find("ul#message_list").html());
		expect(component.find("ul#message_list").children().length).toBe(5);
		await component
			.find("ul#message_list")
			.childAt(0)
			.find("button#delete")
			.simulate("click");
		await component.update();

		expect(axios.delete).toHaveBeenCalledWith(
			"http://localhost:3001/message/1",
			{ id: 1 }
		);
		expect(component.find("ul#message_list").children().length).toBe(4);
	});
});

describe("MessageApp erroring", () => {
	beforeEach(() => {
		axios.post.mockImplementation(() => Promise.reject(errorMock));
		axios.get.mockImplementation(() => Promise.reject(errorMock));
	});

	afterEach(() => {
		axios.post.mockClear();
		axios.get.mockClear();
	});

	it("loads error on get error", async () => {
		const component = await mount(<MessageApp />);
		await component.update();

		expect(axios.get).toHaveBeenCalledTimes(1);
		expect(component.state("error")).toEqual({
			response: { data: "error text from json mock" },
		});
		expect(component.find("#error").text()).toBe(
			"Error: error text from json mock"
		);
	});

	it("loads error on post error", async () => {
		const component = await mount(<MessageApp />);
		component.find("textarea#message_box").simulate("change", {
			target: { value: "bad string" },
		});

		await component.find("form").simulate("submit");
		await component.update();

		expect(axios.post).toHaveBeenCalledTimes(1);
		expect(component.state("error")).toEqual({
			response: { data: "error text from json mock" },
		});
		expect(component.find("#error").text()).toBe(
			"Error: error text from json mock"
		);
	});
});

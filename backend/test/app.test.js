import request from "supertest";
import { expect } from "chai";
import MessageApp from "../app.js";

describe("message API endpoint test", () => {
	it("posts a message", (done) => {
		const data = {
			content: "hello world",
		};
		const res = request(MessageApp)
			.post("/message")
			.send(data)
			.set("Accept", "application/json");

		res.expect(200).end((err, res) => {
			if (err) return done(err);
			expect(res.body[0].content).to.equal("hello world");
			done();
		});
	});

	it("get one message", (done) => {
		const res = request(MessageApp).get("/message/1");

		res.expect(200).end((err, res) => {
			if (err) return done(err);

			expect(res.body.content).to.equal("hello world");
			done();
		});
	});

	it("gets from backend messages", (done) => {
		const res = request(MessageApp).get("/");

		res.expect(200).end((err, res) => {
			if (err) {
				return done(err);
			}
			expect(res.body.length).to.equal(1);
			done();
		});
	});

	it("updates an message", (done) => {
		const data = {
			content: "Hello World",
		};
		const res = request(MessageApp)
			.patch("/message/1")
			.send(data)
			.set("Accept", "application/json");

		res.expect(200).end((err, res) => {
			if (err) done(err);

			expect(res.body.content).to.equal(data.content);
			done();
		});
	});

	it("deletes a message", (done) => {
		const res = request(MessageApp)
			.delete("/message/1")
			.set("Accept", "application/json");

		res.expect(200).end((err, res) => {
			if (err) return done(err);
			expect(res.body.length).to.equal(0);
			done();
		});
	});
});

describe("message API errors correctly", () => {
	it("posts a message errors", (done) => {
		const data = {
			content: "",
		};
		const res = request(MessageApp)
			.post("/message")
			.send(data)
			.set("Accept", "application/json");

		res.expect(404).end((err, res) => {
			if (err) return done(err);
			expect(res.body).to.equal("You can't post an empty message");
			done();
		});
	});

	it("errors if can't find single message", (done) => {
		const res = request(MessageApp).get("/message/1");

		res.expect(404).end((err, res) => {
			if (err) return done(err);

			expect(res.body).to.equal("Message not found in database");
			done();
		});
	});

	it("gets all errors when no messages", (done) => {
		const res = request(MessageApp).get("/");

		res.expect(404).end((err, res) => {
			if (err) {
				return done(err);
			}
			expect(res.body).to.equal("No messages in database");
			done();
		});
	});

	it("errors on bad update", (done) => {
		const data = {
			content: "Hello World",
		};
		const res = request(MessageApp)
			.patch("/message/0")
			.send(data)
			.set("Accept", "application/json");

		res.expect(404).end((err, res) => {
			if (err) done(err);

			expect(res.body).to.equal("You can't post an empty message");
			done();
		});
	});

	it("deletes a message", (done) => {
		const res = request(MessageApp)
			.delete("/message/0")
			.set("Accept", "application/json");

		res.expect(404).end((err, res) => {
			if (err) return done(err);
			expect(res.body).to.equal("Message not found in database");
			done();
		});
	});
});
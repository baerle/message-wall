import express from 'express';
import { router } from './lib/routes.js';
import bodyparser from 'body-parser';
import cors from 'cors';

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(bodyparser.json());
app.use(cors());

app.use(router);

app.listen(3001, () => console.log('Listening on 3001'));

export default app;
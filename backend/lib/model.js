import fs from "fs";
import path from "path";
import { fileURLToPath } from "url";

class MessageApp {
	constructor(filepath) {
		this.filepath = filepath;
		this.messages = filepath ? this.readFromJson() : [];
	}

	post(content) {
		if (!content) return [];
		const item = {
			id: newId(this.messages),
			content: content,
			date: new Date(),
		};
		this.messages.push(item);
		this.writeToJSON();
		return this.messages;
	}

	get(id) {
		return this.messages.filter((message) => message.id === id)[0];
	}

	getAll() {
		return this.messages;
	}

	update(id, content) {
		if (id < 1 || !content) return [];

		const index = this.messages.findIndex((message) => message.id === id);
		this.messages[index].content = content;
		this.writeToJSON();
		return this.messages[index];
	}

	delete(id) {
		if (id < 1) {
			return "Message not found in database";
		}
		this.messages = this.messages.filter((message) => message.id !== id);
		this.writeToJSON();
		return this.messages;
	}

	readFromJson() {
		return JSON.parse(
			fs.readFileSync(
				path.dirname(fileURLToPath(import.meta.url)) +
					path.normalize(this.filepath),
				"utf8",
				(err, data) => {
					if (err) throw err;
				}
			)
		);
	}

	writeToJSON() {
		if (this.filepath) {
			const jsonItem = JSON.stringify(this.messages);
			fs.writeFileSync(
				path.dirname(fileURLToPath(import.meta.url)) +
					path.normalize(this.filepath),
				jsonItem,
				(err) => {
					if (err) throw err;
				}
			);
		}
	}
}

function newId(array) {
	if (array.length > 0) {
		return array[array.length - 1].id + 1;
	} else {
		return 1;
	}
}

export default MessageApp;

import { Router } from "express";
import messageApp from "./controller.js";

const router = Router();

router.get(
	"/",
	async (req, res) =>
		await messageApp
			.getAll()
			.then((messages) => res.json(messages))
			.catch((err) => res.status(404).json(err))
);

router.get(
	"/message/:id",
	async (req, res) =>
		await messageApp
			.get(req.params.id)
			.then((message) => res.json(message))
			.catch((err) => res.status(404).json(err))
);

router.post("/message", async (req, res) => {
	await messageApp
		.post(req.body.content)
		.then((messages) => res.json(messages))
		.catch((err) => res.status(404).json(err));
});

router.patch("/message/:id", async (req, res) => {
	await messageApp
		.patch(req.params.id, req.body.content)
		.then((message) => res.json(message))
		.catch((err) => {
			res.status(404).json(err);
		});
});

router.delete("/message/:id", async (req, res) => {
	await messageApp
		.deleteMessage(req.params.id)
		.then((messages) => {
			res.json(messages);
		})
		.catch((err) => res.status(404).json(err));
});

export { router };

import MessageApp from "./model.js";

let messageApp;

switch (process.env.npm_lifecycle_event) {
	case "test":
		messageApp = new MessageApp("////json///testMessages.json");
		break;
	default:
		messageApp = new MessageApp("////json///messages.json");
		break;
}

function get(id) {
	return new Promise((resolve, reject) => {
		const result = messageApp.get(Number.parseInt(id));
		if (result) {
			resolve(result);
		}
		reject("Message not found in database");
	});
}

function getAll() {
	return new Promise(function (resolve, reject) {
		const result = messageApp.getAll();
		if (result.length !== 0) {
			resolve(result);
		} else {
			reject("No messages in database");
		}
	});
}

function post(content) {
	return new Promise((resolve, reject) => {
		const result = messageApp.post(content);
		if (result.length !== 0) {
			resolve(result);
		} else {
			reject("You can't post an empty message");
		}
	});
}

function patch(id, content) {

	return new Promise((resolve, reject) => {
		const result = messageApp.update(Number.parseInt(id), content);
		if (result.length !== 0) {
			resolve(result);
		} else {
			reject("You can't post an empty message");
		}
	});
}

function deleteMessage(id) {
	return new Promise((resolve, reject) => {
		const result = messageApp.delete(Number.parseInt(id));
		if (result !== "Message not found in database") {
			resolve(result);
		} else {
			reject(result);
		}
	});
}

export default { get, getAll, post, patch, deleteMessage };
